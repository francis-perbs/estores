from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask import current_app
from flask_login import AnonymousUserMixin
from . import db, login_manager

class Permission:
    RECIV_ORDERS = 0x01
    MANAGE_PRODUCTS = 0x02
    MANAGE_ESTABLISHMENTS = 0x04
    ADMINISTER = 0x80

class AnonymousUser(AnonymousUserMixin):
    def can(self, permissions):
        return False

    def is_administrator(self):
        return False

login_manager.anonymous_user = AnonymousUser

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    vendors = db.relationship('Vendor', backref='role', lazy='dynamic')

    def __repr__(self):
        return '<Role %r>' % self.name

    @staticmethod
    def insert_roles():
        roles = {
            'Vendor': (Permission.MANAGE_PRODUCTS | Permission.MANAGE_ESTABLISHMENTS, True),
            'Deliverer': (Permission.RECIV_ORDERS, False),
            'Administrator': (0xff, False),
        }
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.permissions = roles[r][0]
            role.default = roles[r][1]
            db.session.add(role)
        db.session.commit()

class Vendor(db.Model):
    __tablename__ = 'vendors'
    vid = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), index=True, unique=True)
    pword_hash = db.Column(db.String(128))
    confirmed = db.Column(db.Boolean, default=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    name = db.Column(db.String(64))
    contact_no = db.Column(db.Numeric(11))
    address = db.Column(db.String(64))
    img_src = db.Column(db.String(64))
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)
    establishment_count = db.Column(db.Integer, default=1)
    establishments = db.relationship('Establishment', backref='vendor', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Vendor, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['FLASKY_ADMIN']:
                self.role = Role.query.filter_by(permissions=0xff).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def __repr__(self):
        return '<Vendor %r>' % self.name

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.pword_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.pword_hash, password)

    def can(self, permissions):
        return self.role is not None and (self.role.permissions & permissions) == permissions

    def is_administrator(self):
        return self.can(Permission.ADMINISTER)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.vid)

class Establishment(db.Model):
    __tablename__ = 'establishments'
    eid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    address = db.Column(db.String(64))
    etype = db.Column(db.String(64))
    img_src = db.Column(db.String(64))
    vid = db.Column(db.Integer, db.ForeignKey('vendors.vid'))
    sales = db.relationship('Sale', backref='establishment', lazy='dynamic')

    def __repr__(self):
        return '<Establishment %r>' % self.name

class Stock(db.Model):
    __tablename__ = 'stocks'
    eid = db.Column(db.Integer, db.ForeignKey('establishments.eid'), primary_key=True)
    prod_id = db.Column(db.Integer, db.ForeignKey('products.prod_id'), primary_key=True)
    availability = db.Column(db.Boolean, default=True)

class Product(db.Model):
    __tablename__ = 'products'
    prod_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.Text)
    category = db.Column(db.String(64))
    prod_type = db.Column(db.String(64))
    price = db.Column(db.Float)
    img_src = db.Column(db.String(64))

    def __repr__(self):
        return '<Product %r>' % self.name

class SalesDetail(db.Model):
    __tablename__ = 'salesdetails'
    prod_id = db.Column(db.Integer, db.ForeignKey('products.prod_id'), primary_key=True)
    sales_id = db.Column(db.Integer, db.ForeignKey('sales.sales_id'), primary_key=True)
    quantity = db.Column(db.Integer, default=0)

class Sale(db.Model):
    __tablename__ = 'sales'
    sales_id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(), default=datetime.utcnow)
    sum_total = db.Column(db.Float)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.customer_id'))
    eid = db.Column(db.Integer, db.ForeignKey('establishments.eid'))
    delivery_id = db.Column(db.Integer, db.ForeignKey('deliveries.delivery_id'))

    def __repr__(self):
        return '<Sale %r>' % self.sales_id

class Customer(db.Model):
    __tablename__ = 'customers'
    customer_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    address = db.Column(db.String(64))
    mobile_no = db.Column(db.Numeric(11))
    sales = db.relationship('Sale', backref='customer', lazy='dynamic')

    def __repr__(self):
        return '<Customer %r>' % self.name

class Delivery(db.Model):
    __tablename__ = 'deliveries'
    delivery_id = db.Column(db.Integer, primary_key=True)
    provider = db.Column(db.String(64))
    provider_contact_no = db.Column(db.Numeric(11))
    provider_address = db.Column(db.String(64))
    delivery_person = db.Column(db.String(64))
    delivery_contact_no = db.Column(db.Numeric(11))
    sales = db.relationship('Sale', backref='delivery', lazy='dynamic')

    def __repr__(self):
        return '<Delivery %r>' % self.provider
