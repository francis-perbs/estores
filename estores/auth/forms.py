from flask_wtf import FlaskForm
from wtforms import ValidationError, StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Email, Length, Regexp
from wtforms.fields.html5 import EmailField, DecimalField
from ..models import Vendor

class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired(), Length(1,64), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember me')
    #submit = SubmitField('Log In')

class RegistrationForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired(), Length(1,64), Email()])
    full_name = StringField('Full Name', validators=[DataRequired(), Length(1,64), Regexp('^[A-Za-z][A-Za-z. ]*$', 0, 'Names must consist of letters only')])
    contact_no = DecimalField('Mobile Number', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    #submit = SubmitField('Register')

    def validate_email(self, field):
        if Vendor.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')
