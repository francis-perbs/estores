from flask import current_app, render_template
from flask_login import login_required
from . import accounts

@accounts.route('/dashboard', methods=['GET'])
@login_required
def dashboard():
    return render_template('accounts/dashboard.html')

@accounts.route('/profile', methods=['GET'])
@login_required
def profile():
    return render_template('accounts/profile.html')

@accounts.route('/settings', methods=['GET'])
@login_required
def settings():
    return render_template('accounts/settings.html')
