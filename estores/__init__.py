from flask import Flask
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_assets import Environment
from config import config
from .util.assets import bundles

moment = Moment()
db = SQLAlchemy()
assets = Environment()
assets.register(bundles)
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'
#login_manager.login_message = 'Please Log In to access your requested page.'

def create_app(config_name):
    app = Flask(__name__, static_url_path='/static')
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    moment.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    assets.init_app(app)

    from .models import Vendor
    @login_manager.user_loader
    def load_user(vid):
        return Vendor.query.get(int(vid))

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    from .accounts import accounts as accounts_blueprint
    app.register_blueprint(accounts_blueprint)

    return app
