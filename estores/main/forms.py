from flask_wtf import FlaskForm
from wtforms import ValidationError, StringField, SubmitField
from wtforms.validators import DataRequired, Length, Regexp
from ..models import Role, Vendor, Establishment

class VendorProfileForm(FlaskForm):
    name = StringField('Full Name', validators=[Length(0, 64)])
    contact_no = StringField('Contact Number', validators=[DataRequired(), Regexp('^[0-9]+$', 0, '11-digit mobile number must consist of numbers only')])
    address = StringField('Home Address', validators=[Length(0, 64)])
    img_src = StringField('Picture')
    #submit = SubmitField('Update')

class EstablishmentProfileForm(FlaskForm):
    name = StringField('Establishment Name', validators=[Length(0, 64)])
