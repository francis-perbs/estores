from flask import current_app, render_template, url_for, redirect
from flask_login import current_user
from . import main

@main.route('/', methods=['GET'])
def index():
    return render_template('main/index.html')
