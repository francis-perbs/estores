from flask_assets import Bundle

bundles = {
    'uikit_js': Bundle(
        'vendor/jquery/jquery.js',
        'vendor/uikit/js/uikit.js',
        'vendor/uikit/js/uikit-icons.js',
        output='gen/uikit.js'
    ),
    'uikit_css': Bundle(
        'vendor/uikit/css/uikit.css',
        'css/custom.css',
        output='gen/uikit.css'
    ),
}
